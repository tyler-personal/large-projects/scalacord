package scalacord
import scalacord.lib._
import scalacord.lib.implicits._
import scalacord.entities._
import cats.effect._
import cats.syntax.all._
import java.io.OutputStream
import java.io.PrintStream
import cats.effect.unsafe.implicits._
import cats.effect.std.Dispatcher
import java.nio.file.{Paths, Files}
import java.nio.charset.StandardCharsets

given [A,B](using a: A, b: B): (A,B) = (a,b)
given [A,B,C](using a: A, b: B, c: C): (A,B,C) = (a,b,c)
given [A,B,C,D](using a: A, b: B, c: C, d: D): (A,B,C,D) = (a,b,c,d)
given [A,B,C,D,E](using a: A, b: B, c: C, d: D, e: E): (A,B,C,D,E) = (a,b,c,d,e)

import org.javacord.api._
import scalacord.command._


@main def fn = expression.unsafeRunSync()

object Main extends IOApp.Simple:
  override def run: IO[Unit] = expression

def expression: IO[Unit] =
  for
    bot <- DiscordBot(_ / "data" / "token")
//    _   <- bot.commandListener()
    _   <- IO.println("Bot Starting")

    easyPing = Command("ping easy", "Check the bot", _.respond("Pong"))
    potato   = Command("potato ez", "Check potato", _.respond("Potato"))
    fullPing = Command[(String, Int)]("ping full", "Check that the bot works with arguments", (
      Argument("name", "What is your name?"),
      Argument("age", "How old are you?"),
    ), (event, name, age) =>
      for
        _ <- IO.println("full ping occurred")
        _ <- event.respond(s"Hello $name, how's it feel being $age?")
      yield ()
    )

    _ <- bot.listenFor(_.Message)(event =>
      IO.whenA(event.author.name === "Nevoic")(event.respond("Hello, Person."))
    )
    _ <- bot.listenFor(_.Command)(List(fullPing, potato))
  yield ()

  
