package scalacord.entities

import org.javacord.api.event.message.CertainMessageEvent
import org.javacord.api.event.interaction.SlashCommandCreateEvent
import cats.effect.IO
import org.javacord.api.event.message.MessageCreateEvent
import scalacord.lib.implicits._
import org.javacord.api.entity.message.MessageAuthor
import org.javacord.api.entity.channel.TextChannel
import org.javacord.api.entity.user.User
import java.util.concurrent.CompletableFuture
import cats.implicits._

sealed trait Event(javacord: CertainMessageEvent):
  def author = Author(javacord.getMessageAuthor)
  def channel = javacord.getChannel

case class SlashEvent(javacord: SlashCommandCreateEvent): 
  private val responder = javacord.getSlashCommandInteraction.createImmediateResponder
  private val interaction = javacord.getSlashCommandInteraction
  private var responded = false

  val author: User = interaction.getUser

  val channel: Option[TextChannel] = interaction.getChannel

  def respond(message: String): IO[Unit] = {
    if (responded)
      channel.traverse(_.sendMessage(message).toIO).void
      // channel.foreach(_.sendMessage(message).join).pure[IO]
    else
      responded = true
      responder.append(message.asInstanceOf[Object]).respond.toIO.void
//    responder.respond.join
  }
//    channel.map(c => c.sendMessage(message)).getOrElse(CompletableFuture.allOf()).toIO.void

case class TextEvent(javacord: MessageCreateEvent) extends Event(javacord):
  def respond(message: String): IO[Unit] =
    channel.sendMessage(message).join.pure[IO].void

case class Author(javacord: MessageAuthor):
  def name = javacord.getName
  def discriminatedName = javacord.getDiscriminatedName
  def id = javacord.getId

