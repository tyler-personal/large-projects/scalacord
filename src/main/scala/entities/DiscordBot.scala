package scalacord.entities

import java.util.stream.Collectors

import scala.util.chaining._

import cats._
import cats.effect.IO
import cats.implicits._
import cats.syntax.ApplyOps._
import cats.syntax._

import scalacord.lib.implicits._
import scalacord.command.{CommandU, mkTrees, mkCommand}

import org.javacord.api.{DiscordApi, DiscordApiBuilder}
import org.javacord.api.entity.server.Server
import org.javacord.api.listener.message.MessageCreateListener

import os.PathChunk
import os.Path


case class User(name: String, age: Int)
object Test:
  def getUserIdsByLocation(location: String): IO[List[Int]] = ???

  def getUserById(id: Int): IO[User] = ???

  def oldestPersonInUSA: IO[User] =
    for
      ids   <- getUserIdsByLocation("USA") 
      users <- ids.parTraverse(getUserById)
    yield users.maxBy(_.age)

  val x = oldestPersonInUSA.map(_.age)



enum ListenType[FN]:
  case Message extends ListenType[(TextEvent => IO[Unit]) => IO[Unit]]
  case Command extends ListenType[List[CommandU] => IO[Unit]]

case class DiscordBot(api: DiscordApi):
  def servers: List[Server] = api.getServers

  def listenFor[FN](listenType: ListenType.type => ListenType[FN]): FN = listenType(ListenType) match
    case ListenType.Message =>
      (listener: TextEvent => IO[Unit]) => IO(api.addMessageCreateListener(TextEvent(_).pipe(listener(_)))).void
    case ListenType.Command =>
      (commands: List[CommandU]) =>
        val trees = mkTrees(commands.map(c => c.parts -> c).toMap)
        (trees, servers).traverseN(mkCommand(_, _)(this)).void

object DiscordBot:
  def apply(token: Path => Path, workingdir: Path = os.pwd): IO[DiscordBot] =
    DiscordBot(os.read(token(workingdir)))

  def apply(token: String): IO[DiscordBot] =
    DiscordApiBuilder().setToken(token).setAllIntents.login.toIO.map(DiscordBot(_))
