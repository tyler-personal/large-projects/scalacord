package scalacord.command

import java.util.Optional

import scala.compiletime.ops.int.S
import scala.jdk.OptionConverters._
import scala.reflect.runtime.universe._
import scala.util.chaining._

import cats.{Applicative, Functor}
import cats.effect.std.Dispatcher
import cats.effect.unsafe.implicits._
import cats.effect.IO
import cats.implicits._

import scalacord.entities.DiscordBot
import scalacord.entities._
import scalacord.lib.Vect._
import scalacord.lib._
import scalacord.lib.implicits._

import izumi.reflect._
import izumi.reflect.macrortti._

import org.javacord.api.DiscordApi
import org.javacord.api.entity.server.Server
import org.javacord.api.event.interaction.SlashCommandCreateEvent
import org.javacord.api.event.message.CertainMessageEvent
import org.javacord.api.event.message.MessageCreateEvent
import org.javacord.api.interaction.SlashCommand
import org.javacord.api.interaction.SlashCommandBuilder
import org.javacord.api.interaction.SlashCommandInteraction
import org.javacord.api.interaction.SlashCommandInteractionOption
import org.javacord.api.interaction.SlashCommandOption
import org.javacord.api.interaction.SlashCommandOptionChoice
import org.javacord.api.interaction.SlashCommandOptionType

import SlashCommandOptionType._
//import Tuple.{Head, Tail}

type CommandOptionType = String | Boolean | Int
// ServerChannel | Mentionable | Role | User

final case class Argument(
    name: String,
    description: String = "",
    required: Boolean = true,
    choices: List[String] = List()
)

sealed trait CommandInfo(val name: String, val description: String):
  val parts                     = name.split(" ").toList
  val (baseName :: subCommands) = parts

final case class SimpleCommand(
    override val name: String,
    override val description: String,
    listener: SlashEvent => IO[Unit]
) extends CommandInfo(name, description)

extension [A](value: A)
  def |>[B](f: A => B) = f(value)

//def map[F[_]: Functor, A, B](fn: A => B)(fa: F[A])(using F !<:< Function1): F[B] = fa.map(fn)

final case class Command[T <: NonEmptyTuple](
    override val name: String,
    override val description: String,
    val args: Tuple,
    val listener: SlashEvent *: T => IO[Unit]
)(using
    tupleBounded: T <~< CommandOptionType,
    argsBounded: args.type <~< Argument,
    tupleLength: TupleLength[T] =:= TupleLength[args.type],
    tag: Tuple.Map[T, LTag]
) extends CommandInfo(name, description):

  private def tagNames: List[String] => List[SlashCommandOptionType] =
    case Nil     => Nil
    case x :: xs =>
      (x match
        case "String"  => STRING
        case "Int"     => INTEGER
        case "Boolean" => BOOLEAN
        case _         => throw RuntimeException(compiletimeRuntimeMismatch)
      ) :: tagNames(xs)



  val typeAndArgs: List[(SlashCommandOptionType, Argument)] = tag.toList
    |> (_.map(t => t.asInstanceOf[LTag[?]].tag.shortName))
    |> tagNames 
    |> (_.zip(args.toBoundedList))
//  val typeAndArgs: List[(SlashCommandOptionType, Argument)] = tagNames(
//    tag.toList.map(tag => tag.asInstanceOf[LTag[?]].tag.shortName)
//  ).zip(args.toBoundedList)
//    .zip(args.toList.map(_.asInstanceOf[Argument[?]]))

object Command:
  def apply(name: String, description: String, listener: SlashEvent => IO[Unit]) =
    SimpleCommand(name, description, listener)

enum CommandTree(val key: String):
  case Node(override val key: String, children: List[CommandTree]) extends CommandTree(key)
  case Leaf(override val key: String, value: CommandU) extends CommandTree(key)

import CommandTree._

type CommandU     = SimpleCommand | Command[? <: NonEmptyTuple]
type SlashRun     = (Option[SlashCommandInteractionOption], SlashEvent) => IO[Unit]
type SafeSlashRun = (SlashCommandInteractionOption, SlashEvent) => IO[Unit]

extension [K, V](xs: List[(K, V)]) 
  def groupToMap: Map[K, List[V]] = xs.groupBy(_._1).view.mapValues(_.map(_._2)).toMap

def mkTrees(map: Map[List[String], CommandU]): List[CommandTree]                             =
  def rest(key: String) = map.toList.collect { case (k :: x :: xs, v) if k === key => (x :: xs, v) }.toMap

  map.toList.map {
    case (Nil, _)      => throw RuntimeException("This should never occur, the last recurse should've been the leaf.")
    case (k :: Nil, v) => Leaf(k, v)
    case (x :: xs, v)  => Node(x, mkTrees(rest(x)))
  }.distinctBy(_.key)

def mkCommand(root: CommandTree, server: Server)(bot: DiscordBot): IO[SlashCommand] =
  def mk[T](
      tree: CommandTree,
      simpleLeafFn: (String, String) => T,
      complexLeafFn: (String, String, List[SlashCommandOption]) => T,
      nodeFn: (String, String, List[SlashCommandOption]) => T
  ): (T, SlashRun) = tree match
    case Leaf(k, v: SimpleCommand) =>
      (
        simpleLeafFn(k, v.description),
        (_, e) =>
          for
            _   <- IO.println("Simple listener is firing")
            res <- v.listener(e)
          yield res
      )

    case Leaf(k, v: Command[NonEmptyTuple]) =>
//      IO.println(s"mk -- Leaf, Command -- $k").unsafeRunSync()
      (
        complexLeafFn(
          k,
          v.description,
          v.typeAndArgs.map {
            case (t, Argument(n, d, r, Nil)) => SlashCommandOption.create(t, n, d, r)
            case (t, Argument(n, d, r, xs))  =>
              SlashCommandOption.createWithChoices(t, n, d, r, xs.map(x => SlashCommandOptionChoice.create(x, x)))
          }
        ),
        nextOpt(
          k,
          (opt, event) =>
            val args: NonEmptyTuple = v.typeAndArgs.map(_._1).zipWithIndex.foldLeft[Tuple](Tuple()) { case (acc, (e, i)) =>
              def process(f: Int => Optional[?]): Any =
                println(s"@ $i")
                f(i).orElseThrow(() => Exception(commandReceiveMismatch))
              acc ++ Tuple(process(e match
                case STRING  => 
                  println(s"STRING GET ATTEMPT ON ${opt.getOptions.map(_.getName)}")
                  opt.getOptionStringValueByIndex
                case INTEGER => 
                  println("INT GET ATTEMPT")
                  opt.getOptionIntValueByIndex
                case BOOLEAN => 
                  println("BOOLEAN GET ATTEMPT")
                  opt.getOptionBooleanValueByIndex
                case _       => 
                  println("MISSING GET ATTEMPT")
                  throw RuntimeException(compiletimeRuntimeMismatch)
              ))
            }.asInstanceOf[NonEmptyTuple]
            v.listener(event *: args)
        )
      )

    case Node(k, children) =>
//      IO.println(s"mk -- Node -- $k").unsafeRunSync()
      val (ts, fns) = children.map(mkOption).unzip
      (nodeFn(k, k, ts), nextOpt(k, (opt, event) => fns.traverse_(_(opt.getFirstOption.toScala, event))))

  def nextOpt(k: String, f: SafeSlashRun): SlashRun = (opt, event) =>
    opt match
      case Some(o) if o.getName == k => f(o, event)
      //case Some(o)                   => IO.println("nextOpt: NULLED OUT - with opt") >> f(o, event)
      case Some(o)                   => 
        IO.println(s"next OPT: NULLED OUT - with opt // ${o.getName} != ${k}") >> f(o, event)
      case _                         => IO.println("nextOpt: NULLED OUT - without opt")

  def mkOption(tree: CommandTree): (SlashCommandOption, SlashRun) =
    mk(
      tree,
      SlashCommandOption.create(SUB_COMMAND, _, _),
      SlashCommandOption.createWithOptions(SUB_COMMAND, _, _, _),
      SlashCommandOption.createWithOptions(SUB_COMMAND_GROUP, _, _, _)
    )
  val (commandBuilder, fn) = mk(
    root,
    SlashCommand.`with`(_, _),
    SlashCommand.`with`(_, _, _),
    SlashCommand.`with`(_, _, _)
  )
  IO.println(s"mkCommand - ${root.key}").unsafeRunSync()
  for
    commandInstance <- commandBuilder.createForServer(server).toIO
    _ <- IO.println(s"mkCommand: Start -- COMMAND: ${root.key} -- SERVER: ${server.getName}")
    _ <- IO {
      bot.api.addSlashCommandCreateListener { event =>
        (for {
          _ <- IO.println("SlashCommandCreateListener: Start")
          interaction = event.getSlashCommandInteraction
          slashEvent  = SlashEvent(event)
          _ <- IO.println(s"if ${event.getSlashCommandInteraction.getCommandName} == ${commandInstance.getName}")
          _ <- IO.println("SlashCommandCreateListener: Start")
          _ <- IO.whenA(event.getSlashCommandInteraction.getCommandId == commandInstance.getId)(
            for
              _ <- IO.println("1")
              _  = println(2)
              _ <- fn(interaction.getFirstOption.toScala, slashEvent)
              _ <- slashEvent.respond("shrug")
            yield ()
          )
        } yield ()).start.unsafeRunSync()
      }
    }
  yield commandInstance

private def compiletimeRuntimeMismatch                                                               =
  """
    The runtime doesn't support a type denoted by `CommandOptionType`.
    THIS SHOULD NEVER OCCUR. If it does, there is a programming error in scalacord.
    Please report this on the repo.
  """

private def commandReceiveMismatch                                                                   =
  """
      The command setup doesn't match what is setup on Discord.
      Could this be an old command?
  """
//    val creates: List[IO[SlashCommand]] = bot.servers.map(root.createForServer(_))
