package scalacord.lib

import cats.effect.IO
import java.util.concurrent.CompletableFuture
import scala.collection.JavaConverters._
import java.util.stream.Collectors

import scala.language.implicitConversions

object implicits:
  extension [T](future: CompletableFuture[T])
    implicit def toIO: IO[T] = IO.fromCompletableFuture(IO(future))

  extension [T](xs: java.util.Collection[T])
    implicit def toScalaList: List[T] =
      xs.stream.collect(Collectors.toList).asScala.toList

  extension [T](xs: List[T])
    implicit def toJava: java.util.List[T] = xs.asJava

  extension [T](opt: java.util.Optional[T])
    implicit def toScalaOption: Option[T] =
      opt.map(Option(_)).orElse(None)
