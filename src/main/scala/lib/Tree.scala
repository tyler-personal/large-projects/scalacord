package scalacord.lib


enum Tree[Key, Value]:
  case SimpleRoot(key: Key, value: Value)
  case Root(key: Key, children: List[Node[Key, Value] | Leaf[Key, Value]])

  case Node(key: Key, children: List[Node[Key, Value] | Leaf[Key, Value]])
  case Leaf(key: Key, value: Value)
