package scalacord.lib

import scala.annotation.implicitNotFound

import scalacord.lib.Vect._
import scalacord.lib.Nat._

type TupleLength[T <: Tuple] <: Nat = T match
  case EmptyTuple => Nat0
  case x *: xs    => S[TupleLength[xs]]

type IsHomogenous[T <: Tuple] <: Boolean = T match
  case EmptyTuple      => true
  case x *: EmptyTuple => true
  case x *: y *: xs    => And[Is[x, y], IsHomogenous[y *: xs]]


type <~<[T <: Tuple, A] = IsBounded[T, A] =:= true

type IsBounded[T <: Tuple, A] <: Boolean = T match
  case EmptyTuple => true
  case A *: xs    => IsBounded[xs, A]
  case x *: xs    => false

type And[A <: Boolean, B <: Boolean] <: Boolean = (A, B) match
  case (true, true) => true
  case ?            => false

type Or[A <: Boolean, B <: Boolean] <: Boolean = (A, B) match
  case (false, false) => false
  case ?              => true

type TupleToVector[T <: Tuple, A] = T match
  case EmptyTuple => Vect[0, Nothing]
  case A *: xs    => A ::: Vect[TupleLength[xs], A]

type ~+[N <: Nat, M <: Nat] <: Nat = M match
  case Nat0  => N
  case S[mm] => S[N] ~+ mm 

type +~[N <: Nat, M <: Nat] <: Nat = N match
  case Nat0  => M
  case S[nn] => nn +~ S[M]

  // 5 + 2
  // S[4] => S[2] +~ 4
  // 3 + 4
  // S[2] => S[3] +~ 2
  // 4 + 2
  // S[3] => S[]

extension [T <: Tuple](tuple: T)
  def toVector[A](using T <~< A): Vect[TupleLength[T], A] = 
    def convert[T <: Tuple](tuple: Tuple): Vect[TupleLength[T], A] = (tuple match
      case _: EmptyTuple => VNil
      case x *: xs       => (x.asInstanceOf[A] ::: convert(xs))
    ).asInstanceOf[Vect[TupleLength[T], A]]

    convert(tuple)

extension (tuple: Tuple)
  def toBoundedList[A](using tuple.type <~< A): List[A] = tuple.toList.asInstanceOf[List[A]]

// def tupleToVector[T <: Tuple, A](tuple: T): TupleToVector[T, A] = tuple match
//   case _: EmptyTuple => VNil
//   case x *: xs       => :::(x, tupleToVector[xs.type, A](xs))

type Is[A, B] <: Boolean = A match
  case B => true
  case ? => false
