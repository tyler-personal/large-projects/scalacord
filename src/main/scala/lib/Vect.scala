package scalacord.lib
//import scala.compiletime.ops.int.S

enum Nat:
  case Z
  case S[N <: Nat](n: N)

import Nat._

enum Vect[N <: Nat, A]:
  case VNil[A]() extends Vect[Nat0, A]
  case :::[N <: Nat, A] (head: A, tail: Vect[N, A]) extends Vect[S[N], A]
//  case ::+[N <: Int, A] (init: Vect[N, A], item: A) extends Vect[S[N], A]

//type NonEmptyVect[N <: Int, A] = :::[N, A]

import Vect._

extension [N <: Nat, A](as: Vect[N, A])
  def map[B](f: A => B): Vect[N, B] = as match
    case VNil() => VNil()
    case x ::: xs => f(x) ::: xs.map(f)
  
  def zip[B](bs: Vect[N, B]): Vect[N, (A, B)] = (as, bs) match
    case (VNil(), VNil()) => VNil()
    case (x ::: xs, y ::: ys) => (x, y) ::: xs.zip(ys)

  def size: Int = as match
    case VNil()   => 0
    case _ ::: xs => 1 + xs.size
  
  def toList: List[A] = as.match
    case VNil()   => Nil
    case x ::: xs => x :: xs.toList

  def prepend[M <: Nat](aas: Vect[M, A]): Vect[M +~ N , A] = aas.append(as)

  def append[M <: Nat](aas: Vect[M, A]): Vect[N +~ M, A] = ((as, aas) match
    case (VNil(), VNil()) => VNil()
    case (VNil(), ys)     => ys
    case (x ::: xs, ys)   => (x ::: xs.append(ys)).asInstanceOf[Vect[N +~ M, A]]
  )// .asInstanceOf[Vect[N + M, A]]

  // def appendE[M <: Int](aas: VNil.type)(check: M =:= 0): Vect[N, A] = as
  // def appendS[M <: Int](aas: NonEmptyVect[S[M], A]): Vect[N + S[M], A] = (aas match
  //   case x ::: VNil => as.appendE(VNil)
  //   case x ::: (xs: NonEmptyVect[M, A])   => as.appendS(xs) ::+ x
  // ) ::+ aas.head
//  as.append(aas.tail) ::+ aas.head

extension [A](a: A)
  def :::[N <: Nat](as: Vect[N, A]): Vect[S[N], A] = Vect.:::(a, as)

//extension[N <: Nat, A](as: Vect[N, A])

//  def ::+(a: A): Vect[S[N], A] = Vect.::+(as, a)

type Nat0 = Z.type
type Nat1 = S[Nat0]
type Nat2 = S[Nat1]
type Nat3 = S[Nat2]
type Nat4 = S[Nat3]
type Nat5 = S[Nat4]
type Nat6 = S[Nat5]
type Nat7 = S[Nat6]
type Nat8 = S[Nat7]
type Nat9 = S[Nat8]

object Vect:
  def apply[T <: Tuple, A](t: T): Vect[TupleLength[T], A] = ???
  def apply[A](): Vect[Nat0, A] = VNil()
  def apply[A](a: A): Vect[Nat1, A] = a ::: VNil()
  def apply[A](a: A, b: A): Vect[Nat2, A] = a ::: Vect(b)
  def apply[A](a: A, b: A, c: A): Vect[Nat3, A] = a ::: Vect(b, c)
  def apply[A](a: A, b: A, c: A, d: A): Vect[Nat4, A] = a ::: Vect(b, c, d)
  def apply[A](a: A, b: A, c: A, d: A, e: A): Vect[Nat5, A] = a ::: Vect(b, c, d, e)
  def apply[A](a: A, b: A, c: A, d: A, e: A, f: A): Vect[Nat6, A] = a ::: Vect(b, c, d, e, f)
  def apply[A](a: A, b: A, c: A, d: A, e: A, f: A, g: A): Vect[Nat7, A] = a ::: Vect(b, c, d, e, f, g)
  def apply[A](a: A, b: A, c: A, d: A, e: A, f: A, g: A, h: A): Vect[Nat8, A] = a ::: Vect(b, c, d, e, f, g, h)
  def apply[A](a: A, b: A, c: A, d: A, e: A, f: A, g: A, h: A, i: A): Vect[Nat9, A] = a ::: Vect(b, c, d, e, f, g, h, i)
