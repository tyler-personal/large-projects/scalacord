val scala3Version = "3.0.0"

lazy val root = project
  .in(file("."))
  .settings(
    name := "Scalacord",
    version := "0.1.0",
    scalaVersion := scala3Version,

    // Compile / scalaSource := baseDirectory.value / "src" / "code",
    // scalaCompilerBridgeSource :=
    //   ("org.scala-sbt" % "compiler-interface" % "0.13.15" % "component").sources
    libraryDependencies ++= Seq(
      "dev.zio" %% "izumi-reflect" % "1.1.3",
      "org.javacord" % "javacord" % "3.3.2",
      "com.lihaoyi" %% "os-lib" % "0.7.8",
      "org.typelevel" %% "cats-core" % "2.6.1",
      "org.typelevel" %% "cats-effect" % "3.1.1",
      "com.novocode" % "junit-interface" % "0.11" % "test",
      "com.lihaoyi" %% "pprint" % "0.7.0",
      "com.lihaoyi" % "ammonite" % "2.5.2" cross CrossVersion.full
    )
  )
